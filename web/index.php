<?php
/**
 * @link http://zenothing.com/
 */

defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

require __DIR__ . '/../boot.php';
require __DIR__ . '/../web.php';

(new yii\web\Application($config))->run();
