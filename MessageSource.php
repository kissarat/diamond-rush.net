<?php
/**
 * @link http://zenothing.com/
 */

namespace app;


use Yii;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\i18n\DbMessageSource;

class MessageSource extends DbMessageSource {
    protected function loadMessagesFromDb($category, $language) {
        $mainQuery = new Query();
        $mainQuery->select(['t1.message message', 't2.translation translation'])
            ->from(["$this->sourceMessageTable t1", "$this->messageTable t2"])
            ->where('t1.id = t2.id AND t1.category = :category AND t2.language = :language AND name IS NULL')
            ->params([':category' => $category, ':language' => $language]);

        $fallbackLanguage = substr($language, 0, 2);
        if ($fallbackLanguage != $language) {
            $fallbackQuery = new Query();
            $fallbackQuery->select(['t1.message message', 't2.translation translation'])
                ->from(["$this->sourceMessageTable t1", "$this->messageTable t2"])
                ->where('t1.id = t2.id AND t1.category = :category AND t2.language = :fallbackLanguage')
                ->andWhere("t2.id NOT IN (SELECT id FROM $this->messageTable WHERE language = :language)")
                ->params([':category' => $category, ':language' => $language, ':fallbackLanguage' => $fallbackLanguage]);

            $mainQuery->union($fallbackQuery, true);
        }

        $messages = $mainQuery->createCommand($this->db)->queryAll();

        return ArrayHelper::map($messages, 'message', 'translation');
    }

    public function resetCache($category, $language) {
        Yii::$app->cache->delete([
            parent::class,
            $category,
            $language
        ]);
    }
}
