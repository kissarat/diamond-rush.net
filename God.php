<?php
/**
 * @link http://zenothing.com/
 */

namespace app;

use Yii;
use yii\bootstrap\Html;

class God {
    const CACHE_KEY = 'texts';
    protected static $texts;

    public static function text($name) {
        if (!static::$texts) {
            static::$texts = Yii::$app->cache->get(static::CACHE_KEY);
            if (!static::$texts) {
                static::$texts = SQL::queryKeyed('SELECT "name", "message" AS en, translation AS ru FROM translation');
                Yii::$app->cache->set(static::CACHE_KEY, static::$texts);
            }
        }
        return isset(static::$texts[$name][Yii::$app->language]) ? static::$texts[$name][Yii::$app->language] : '';
    }

    public static function resetTextCache() {
        Yii::$app->cache->delete(static::CACHE_KEY);
    }

    public static function block($name) {
        $text = static::text($name);
        if (!Yii::$app->user->getIsGuest() && Yii::$app->user->identity->isManager()) {
            $text = Html::a('', ['/translation/text', 'name' => $name], [
                'class' => 'glyphicon glyphicon-pencil',
            ])
                . '<br/>' . $text;
        }
        return Html::tag('div', $text, [
            'class' => 'text',
            'data-name' => $name
        ]);
    }
}
