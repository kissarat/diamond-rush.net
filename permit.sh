#!/bin/bash

mkdir runtime
chown :www-data runtime
chmod 771 runtime
mkdir web/assets
chown :www-data web/assets
chmod 771 web/assets
chown :www-data models
chmod 771 models
chown :www-data models/search
chmod 771 models/search
chown :www-data controllers
chmod 771 controllers
chown :www-data views
chmod 771 views

