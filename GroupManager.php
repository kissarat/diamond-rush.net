<?php
/**
 * @link http://zenothing.com/
*/

namespace app;


use app\models\User;
use yii\base\NotSupportedException;
use yii\rbac\ManagerInterface;

/**
 * @author Taras Labiak <kissarat@gmail.com>
 */
class GroupManager implements ManagerInterface {

    public function checkAccess($userId, $permissionName, $params = [])
    {
        /* @var User $user */
        $user = User::findOne($userId);
        if (!$user) {
            return false;
        }
        if ('manage' == $permissionName && $user->isManager()) {
            return true;
        }
        if ('admin' == $permissionName && $user->isAdmin()) {
            return true;
        }
        return false;
    }

    public function createRole($name)               { throw new NotSupportedException(); }
    public function createPermission($name)          { throw new NotSupportedException(); }
    public function add($object)                     { throw new NotSupportedException(); }
    public function remove($object)                  { throw new NotSupportedException(); }
    public function update($name, $object)           { throw new NotSupportedException(); }
    public function getRole($name)                   { throw new NotSupportedException(); }
    public function getRoles()                       { throw new NotSupportedException(); }
    public function getRolesByUser($userId)          { throw new NotSupportedException(); }
    public function getPermission($name)             { throw new NotSupportedException(); }
    public function getPermissions()                 { throw new NotSupportedException(); }
    public function getPermissionsByRole($roleName)  { throw new NotSupportedException(); }
    public function getPermissionsByUser($userId)    { throw new NotSupportedException(); }
    public function getRule($name)                   { throw new NotSupportedException(); }
    public function getRules()                       { throw new NotSupportedException(); }
    public function addChild($parent, $child)        { throw new NotSupportedException(); }
    public function removeChild($parent, $child)     { throw new NotSupportedException(); }
    public function removeChildren($parent)          { throw new NotSupportedException(); }
    public function hasChild($parent, $child)        { throw new NotSupportedException(); }
    public function getChildren($name)               { throw new NotSupportedException(); }
    public function assign($role, $userId)           { throw new NotSupportedException(); }
    public function revoke($role, $userId)           { throw new NotSupportedException(); }
    public function revokeAll($userId)               { throw new NotSupportedException(); }
    public function getAssignment($roleName, $userId){ throw new NotSupportedException(); }
    public function getAssignments($userId)          { throw new NotSupportedException(); }
    public function removeAll()                      { throw new NotSupportedException(); }
    public function removeAllPermissions()           { throw new NotSupportedException(); }
    public function removeAllRoles()                 { throw new NotSupportedException(); }
    public function removeAllRules()                 { throw new NotSupportedException(); }
    public function removeAllAssignments()           { throw new NotSupportedException(); }
}
