CREATE TABLE source_message (
  id SERIAL PRIMARY KEY,
  "name" VARCHAR(24),
  "category" VARCHAR(32) DEFAULT 'app',
  message TEXT
);
CREATE UNIQUE INDEX message_id ON "source_message" USING btree ("id");


CREATE TABLE message (
  id INT,
  language VARCHAR(16) DEFAULT 'ru',
  translation VARCHAR(256),
  PRIMARY KEY (id, language),
  CONSTRAINT fk_message_source_message FOREIGN KEY (id)
  REFERENCES source_message (id) ON DELETE CASCADE ON UPDATE RESTRICT
);


CREATE VIEW translation AS
  SELECT s.id, "name", message, translation
  FROM source_message s JOIN message t ON s.id = t.id;


CREATE TABLE "user" (
  id SERIAL PRIMARY KEY NOT NULL,
  name VARCHAR(24) NOT NULL,
  account DECIMAL(8,2) NOT NULL DEFAULT '0.00',
  email VARCHAR(48) NOT NULL,
  hash CHAR(60),
  auth CHAR(64) UNIQUE,
  code CHAR(64),
  duration INT  NOT NULL DEFAULT 60,
  status SMALLINT NOT NULL,
  perfect VARCHAR(9),
  skype VARCHAR(32),
  ref_name VARCHAR(24),
  data BYTEA
);
CREATE UNIQUE INDEX user_id ON "user" USING btree ("id");
CREATE UNIQUE INDEX user_name ON "user" USING btree ("name");
ALTER TABLE "user" ADD CONSTRAINT user_ref FOREIGN KEY (ref_name) REFERENCES "user"("name") ON DELETE NO ACTION;

INSERT INTO "user"(name, email, status) VALUES ('admin', 'lab_tas@ukr.net', 1);


CREATE TABLE "journal" (
  id SERIAL PRIMARY KEY NOT NULL,
  type VARCHAR(16) NOT NULL,
  event VARCHAR(16) NOT NULL,
  object_id INT,
  data TEXT,
  user_name VARCHAR(24),
  time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  ip INET,
  CONSTRAINT journal_user FOREIGN KEY (user_name)
  REFERENCES "user"(name)
  ON DELETE CASCADE ON UPDATE CASCADE
);


CREATE TABLE "feedback" (
  id SERIAL PRIMARY KEY NOT NULL,
  username VARCHAR(24) NOT NULL,
  email VARCHAR(48),
  subject VARCHAR(256) NOT NULL,
  content TEXT NOT NULL
);
CREATE UNIQUE INDEX feedback_id ON "feedback" USING btree ("id");


CREATE TABLE "review" (
  id SERIAL PRIMARY KEY NOT NULL ,
  user_name VARCHAR(24) NOT NULL,
  content TEXT NOT NULL,
  CONSTRAINT review_user FOREIGN KEY (user_name)
  REFERENCES "user"(name)
  ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE UNIQUE INDEX review_id ON "review" USING btree ("id");


CREATE TABLE "invoice" (
  id SERIAL PRIMARY KEY NOT NULL,
  user_name VARCHAR(24) NOT NULL,
  amount DECIMAL(8, 2) NOT NULL,
  batch BIGINT,
  status VARCHAR(16) DEFAULT 'create',
  CONSTRAINT invoice_user FOREIGN KEY (user_name)
  REFERENCES "user"(name)
  ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT amount CHECK (amount <> 0)
);
CREATE UNIQUE INDEX invoice_id ON "invoice" USING btree ("id");


CREATE TABLE "type" (
  id SERIAL PRIMARY KEY NOT NULL,
  name VARCHAR(256),
  stake DECIMAL(8,2) NOT NULL,
  income DECIMAL(8,2),
  pillow DECIMAL(8,2),
  degree SMALLINT NOT NULL,
  invest_id SMALLINT,
  reinvest SMALLINT,
  next_id SMALLINT,
  profit NUMERIC(8,2),
  enabled BOOL DEFAULT TRUE
);

INSERT INTO "type"
  (id, name,           stake,  income,  pillow, degree, invest_id, reinvest, next_id, profit) VALUES
  (9, 'Express #2',    400.0,  1000.0,  0,      4,      9,         1,        null,    200.0),
  (8, 'Express #1',    125.0,  400.0,   0,      5,      8,         1,        null,    100.0),
  (7, 'Brilliant',     5000.0, 18800.0, 1180.0, 4,      1,         1,        null,    null),
  (6, 'Diamond',       2500.0, 4360.0,  620.0,  4,      1,         1,        7,       null),
  (5, 'Ruby',          1000.0, 1180.0,  300.0,  4,      1,         1,        6,       null),
  (4, 'Emerald',       400.0,  440.0,   140.0,  4,      1,         1,        5,       null),
  (3, 'Topaz',         150.0,  120.0,   60.0,   4,      1,         1,        4,       null),
  (2, 'Cubic zirconia',70.0,   90.0,    20.0,   4,      1,         1,        3,       null),
  (1, 'Amethyst',      20.0,   null,    null,   4,      null,      null,     2,       10.0);


CREATE TABLE "account" (
  "pillow" NUMERIC(8,2) NOT NULL DEFAULT 0,
  "profit" NUMERIC(8,2) NOT NULL DEFAULT 0
);
INSERT INTO "account" VALUES (0, 0);


CREATE TABLE "node" (
  id SERIAL PRIMARY KEY NOT NULL,
  user_name VARCHAR(24) NOT NULL,
  type_id SMALLINT  NOT NULL,
  count SMALLINT NOT NULL,
  reinvest_from SMALLINT,
  time INT  NOT NULL,
  CONSTRAINT node_user FOREIGN KEY (user_name)
  REFERENCES "user"(name)
  ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT node_type FOREIGN KEY (type_id)
  REFERENCES "type"(id)
  ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "count" CHECK (count >= 0)
);
CREATE UNIQUE INDEX node_id ON "node" USING btree ("id");


CREATE VIEW "matrix" AS
  SELECT n.id as id, type_id, user_name, count(*) as count, n.reinvest_from, "time"
  FROM node n JOIN type t ON n.type_id = t.id
  GROUP BY n.reinvest_from, "time", user_name, type_id, n.id;


CREATE TABLE "archive" (
  id SERIAL PRIMARY KEY NOT NULL,
  node_id INT  NOT NULL,
  user_name VARCHAR(24) NOT NULL,
  type_id SMALLINT  NOT NULL,
  reinvest_from SMALLINT ,
  time INT NOT NULL,
  CONSTRAINT archive_user FOREIGN KEY (user_name)
  REFERENCES "user"(name)
  ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE UNIQUE INDEX archive_id ON "archive" USING btree ("id");


CREATE TABLE "faq" (
  "id" SERIAL PRIMARY KEY,
  "question" VARCHAR(256) NOT NULL,
  "answer" TEXT NOT NULL
);


CREATE TABLE "article" (
  "id" SERIAL PRIMARY KEY,
  "title" VARCHAR(256) NOT NULL,
  "content" TEXT NOT NULL
);
CREATE UNIQUE INDEX article_id ON "article" USING btree ("id");


CREATE TABLE "visit_agent" (
  "id" SERIAL PRIMARY KEY,
  "agent" VARCHAR(200),
  "ip" INET
);
CREATE INDEX visit_agent_agent ON "visit_agent" USING btree ("agent");
CREATE INDEX visit_agent_ip ON "visit_agent" USING btree ("ip");


CREATE TABLE "visit_path" (
  "id" SERIAL PRIMARY KEY,
  "agent_id" INT NOT NULL,
  "path" VARCHAR(80) NOT NULL,
  "spend" SMALLINT,
  "time" TIMESTAMP DEFAULT current_timestamp,
  CONSTRAINT user_agent FOREIGN KEY (agent_id)
  REFERENCES "visit_agent"("id")
  ON DELETE CASCADE ON UPDATE CASCADE
);


CREATE VIEW "visit" AS
  SELECT p.id as id, agent_id, spend, "path", "time", ip, agent FROM visit_path p
    JOIN visit_agent a ON agent_id = a.id;
