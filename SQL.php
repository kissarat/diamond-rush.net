<?php
/**
 * @link http://zenothing.com/
 */

namespace app;


use PDO;
use Yii;

/**
 * @author Taras Labiak <kissarat@gmail.com>
 */
class SQL {
    public static function query($sql, $params = null) {
        $command = Yii::$app->db->createCommand($sql, $params);
        $command->execute();
        return $command;
    }

    public static function queryObject($sql, $params = null) {
        return static::query($sql, $params)->pdoStatement->fetchObject();
    }

    public static function queryColumn($sql, $params = null) {
        return static::query($sql, $params)->queryColumn();
    }

    public static function queryCell($sql, $params = null) {
        return static::query($sql, $params)->pdoStatement->fetchColumn();
    }

    public static function queryAll($sql, $params = null, $mode = null) {
        return static::query($sql, $params)->pdoStatement->fetchAll($mode);
    }

    public static function queryKeyed($sql, $params = null) {
        return static::query($sql, $params)->pdoStatement->fetchAll(PDO::FETCH_UNIQUE);
    }
}
