<?php
/**
 * @link http://zenothing.com/
*/
use yii\console\Application;

require_once __DIR__ . '/../boot.php';

$app = new Application($config);

//SET FOREIGN_KEY_CHECKS=0;
//$app->db->createCommand('SET FOREIGN_KEY_CHECKS=0')->execute();

$length = (int) $argv[1];
$transaction = Yii::$app->db->beginTransaction();
$names = [];
for($i = 2; $i < $length; $i++) {
    $name = 'user' . $i;
    $ref_name = array_rand($names);
    $ref_name = $ref_name && rand(1, 10) > 2 ? $names[$ref_name] : null;
    $app->db->createCommand('INSERT INTO "user"("name", email, status, ref_name) VALUES (:name, :email, 2, :ref_name)', [
        ':name' => $name,
        ':email' => $name . '@yopmail.com',
        ':ref_name' => $ref_name
    ])->execute();
    $names[] = $name;
}

$app->db->createCommand('UPDATE "user" SET "account" = 1000,
"hash" = \'$2y$10$zEiSdGHD2q9fRtljNONCbuj15hjLMNTU71IaM5PcR503kNz3VfC7W\'')->execute();
$transaction->commit();
