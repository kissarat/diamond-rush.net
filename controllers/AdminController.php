<?php
/**
 * @link http://zenothing.com/
 */

namespace app\controllers;


use app\behaviors\Access;
use app\models\ImportForm;
use Yii;
use yii\web\Controller;
use yii\web\UploadedFile;

/**
 * @author Taras Labiak <kissarat@gmail.com>
 */
class AdminController extends Controller {
    public function behaviors() {
        return [
            'access' => [
                'class' => Access::class,
                'manager' => ['visit'],
                'admin' => ['import'],
            ]
        ];
    }

    public function actionImport() {
        $model = new ImportForm();
        $result = null;
        if (Yii::$app->request->isPost) {
            $model->files = UploadedFile::getInstances($model, 'files');
            $result = $model->upload();
            $result = empty($result) ? null : json_encode($result, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
        }

        return $this->render('import', [
            'model' => $model,
            'result' => $result
        ]);
    }
}
