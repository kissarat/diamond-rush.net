<?php
/**
 * @link http://zenothing.com/
 */

namespace app\controllers;

use app\behaviors\Access;
use app\models\Invoice;
use app\models\Matrix;
use app\models\Node;
use app\models\search\Income as IncomeSearch;
use app\models\User;
use app\models\Type;
use app\SQL;
use Exception;
use PDO;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\db\Query;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * @author Taras Labiak <kissarat@gmail.com>
 */
class MatrixController extends Controller
{
    public function behaviors() {
        return [
            'access' => [
                'class' => Access::className(),
                'manager' => ['create', 'update', 'delete', 'up', 'rise']
            ],

            'cache' => [
                'class' => 'yii\filters\PageCache',
                'only' => ['index', 'income', 'invest'],
                'duration' => 600,
                'enabled' => false,
                'variations' => [
                    array_merge(['page' => '1', 'size' => '15', 'order' => 'asc'], $_GET),
                    Yii::$app->user->isGuest
                ],
                'dependency' => [
                    'class' => 'yii\caching\DbDependency',
                    'sql' => 'SELECT max("time") + count(*) FROM "node"',
                ],
            ],
        ];
    }

    protected function matrix($page = 1, $size = 15, $order = 'asc') {
        $count = Matrix::find()->select('count(*) as "count"')->groupBy('type_id')->column();
        $count = empty($count) ? 0 : (int) max($count);
        if ((Yii::$app->user->isGuest || !Yii::$app->user->identity->isManager()) && $size > 100) {
            return $this->redirect(['index', 'size' => 15]);
        }
        $pages = [
            'totalCount' => $count,
            'pageSize' => $size,
            'pageSizeParam' => 'size',
            'page' => $page - 1
        ];

        $pages = new Pagination($pages);

        /** @var Query $query */
        $query = null;
        $types = Type::all();
        $order = 'asc' == $order ? SORT_ASC : SORT_DESC;
        foreach($types as $type) {
            $subquery = Matrix::find()
                ->where(['type_id' => $type->id])
                ->offset($pages->offset)
                ->limit($pages->limit)
                ->orderBy(['time' => $order, 'id' => $order]);
            if ($query) {
                $query->union($subquery);
            }
            else {
                $query = $subquery;
            }
        }

        return [
            'matrix' => $query->all(),
            'pages' => $pages
        ];
    }

    public function actionMatrix($page = 1, $size = 15, $order = 'asc') {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $this->matrix($page, $size, $order);
    }

    public function actionIndex($page = 1, $size = 15, $order = 'asc') {
        $result = $this->matrix($page, $size, $order);
        $table = [];
        foreach($result['matrix'] as $invest) {
            if (empty($table[$invest->type_id - 1])) {
                $table[$invest->type_id - 1] = [];
            }
            $table[$invest->type_id - 1][] = $invest->attributes;
        }

        return $this->render('index', [
            'models' => $table,
            'pages' => $result['pages']
        ]);
    }

    public function actionPlan() {
        $dataProvider = new ActiveDataProvider([
            'query' => Type::find()->where(['enabled' => true]),
            'sort' => [
                'defaultOrder' => ['id' => SORT_ASC]
            ]
        ]);

        $participants = SQL::queryAll('SELECT type_id, count(*) as count
            FROM (SELECT type_id, user_name FROM node GROUP BY type_id, user_name) t GROUP BY type_id', null, PDO::FETCH_KEY_PAIR);

        foreach(Type::all() as $type) {
            if (empty($participants[$type->id])) {
                $participants[$type->id] = 0;
            }
        }

        return $this->render('plan', [
            'dataProvider' => $dataProvider,
            'participants' => $participants
        ]);
    }

    public function actionInvest($user = null, $id = null) {
        $parent = $id ? $this->findNode($id) : null;
        $query = Node::find()
            ->with('type')
            ->orderBy(['time' => SORT_ASC, 'id' => SORT_ASC]);
        if ($user) {
            $query->andWhere(['user_name' => $user]);
        }
        elseif ($parent) {
            $query
                ->andWhere('"time" < :time', [':time' => $parent->time])
                ->andWhere(['type_id' => $parent->type_id]);
        }
        return $this->render('invest', [
            'dataProvider' => new ActiveDataProvider([
                'query' => $query
            ]),
            'parent' => $parent
        ]);
    }

    public function actionRise() {
        $transaction = Yii::$app->db->beginTransaction();
        do {
            $i = Node::rise();
        }
        while($i > 0);
        $transaction->commit();
        return $this->redirect(['/user/account']);
    }

    public function actionUp($id) {
        $invest = $this->findNode($id);
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $invest->up();
            $transaction->commit();
        }
        catch (Exception $ex) {
            $transaction->rollBack();
            Yii::$app->session->setFlash('error', $ex->getMessage());
        }
        return $this->redirect(['invest', 'id' => $id]);
    }

    public function actionIncome() {
        $searchModel = new IncomeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('income', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }

    public function actionView($id) {
        $model = $this->findModel($id);

        if (!Yii::$app->user->isGuest &&
            Yii::$app->user->identity->isManager() &&
            $model->load(Yii::$app->request->post())) {
            $model->save();
        }

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    public function actionCreate($id = null) {
        $model = new Node();
        if ($id) {
            $base = $this->findNode($id);
            $model->time = $base->time - 1;
            $model->type_id = $base->type_id;
        }
        else {
            $model->time = time();
        }

        $is_post = $model->load(Yii::$app->request->post());
        if ($is_post) {
            $model->count = $model->type->degree;
            if ($is_post && $model->validate()) {
                if (!$model->reinvest_from) {
                    $model->reinvest_from = null;
                }
                if ($model->save(false)) {
                    return $this->redirect(['invest', 'id' => $model->id]);
                }
            }
            else {
                Yii::$app->session->setFlash('error', json_encode($model->errors, JSON_UNESCAPED_UNICODE));
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id) {
        $model = $this->findNode($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['invest', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id) {
        $this->findNode($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionOpen($id) {
        /** @var User $me */
        $me = Yii::$app->user->identity;
        $node = new Node([
            'user' => Yii::$app->user->identity,
            'type_id' => $id
        ]);

        if ($node->user->account < $node->type->stake) {
            Yii::$app->session->setFlash('error', Yii::t('app', Invoice::$statuses['insufficient_funds']));
            return $this->redirect(['plan']);
        }

        $transaction = Yii::$app->db->beginTransaction();
        if (!$me->hasNodes()) {
            $referral = $me->referral;
            if ($referral) {
                if ($referral->hasNodes()) {
                    $referral->account += Type::get($id)->stake * 0.075;
                    $referral->update(false, ['account']);
                } else {
                    Yii::$app->session->setFlash('info', Yii::t('app', 'The referral did not receive a bonus'));
                }
            }
        }

        $node->user->account -= $node->type->stake;
        try {
            if ($node->user->update(true, ['account']) && $node->invest()) {
                do {
                    $i = Node::rise();
                }
                while($i > 0);
                $transaction->commit();
                Yii::$app->session->setFlash('success', Yii::t('app', 'The plan is open'));
            }
        }
        catch(Exception $ex) {
            $transaction->rollBack();
            Yii::$app->session->setFlash('error', $ex->getMessage());
        }

        return $this->redirect(['invest', 'user' => Yii::$app->user->identity->name]);
    }

    public function actionNode($id) {

        return $this->render('node', [
            'model' => Node::findOne($id)
        ]);
    }

    /**
     * Finds the Type model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Type the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Type::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Type model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Node the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findNode($id) {
        if (($model = Node::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
