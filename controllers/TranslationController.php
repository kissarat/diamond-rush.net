<?php
/**
 * @link http://zenothing.com/
 */

namespace app\controllers;

use app\behaviors\Access;
use app\God;
use Yii;
use app\models\Translation;
use app\models\search\Translation as TranslationSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * @author Taras Labiak <kissarat@gmail.com>
 */
class TranslationController extends Controller
{
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],

            'access' => [
                'class' => Access::className(),
                'manager' => ['index', 'view', 'create', 'update', 'delete', 'text', 'reset'],
            ],
        ];
    }

    public function actionIndex() {
        $searchModel = new TranslationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id) {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }


    public function actionCreate($name = null) {
        $model = new Translation(['name' => $name]);

        if ($model->load(Yii::$app->request->post())) {
            $command = Yii::$app->db->createCommand('INSERT INTO source_message("message") VALUES (:message) RETURNING id', [
                ':message' => $model->message
            ]);
            $command->execute();
            $id = (int) $command->pdoStatement->fetchColumn();
            Yii::$app->db->createCommand('INSERT INTO message(id, translation) VALUES (:id, :translation)', [
                ':id' => $id,
                ':translation' => $model->translation
            ])->execute();
            return $this->redirect(['view', 'id' => $id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdate($id)  {
        $model = $this->findModel($id);
        return $this->edit($model);
    }

    public function actionText($name)  {
        /** @var Translation $model */
        $model = Translation::findOne(['name' => $name]);
        if (!$model) {
            return $this->redirect(['create', 'name' => $name]);
        }
        return $this->edit($model);
    }

    protected function edit(Translation $model) {
        $model->name = trim($model->name);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            Yii::$app->db->createCommand('UPDATE source_message SET "message" = :message, "name" = :name WHERE id = :id', [
                ':id' => $model->id,
                ':message' => $model->message,
                ':name' => empty($model->name) ? null : $model->name
            ])->execute();
            Yii::$app->db->createCommand('UPDATE "message" SET translation = :translation WHERE id = :id', [
                ':id' => $model->id,
                ':translation' => $model->translation
            ])->execute();
            return $this->redirect(['index']);
        }
        $this->reset();
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    protected function reset() {

        God::resetTextCache();
        Yii::$app->i18n->getMessageSource('app')->resetCache('app', 'en');
        Yii::$app->i18n->getMessageSource('app')->resetCache('app', 'ru');
    }

    public function actionReset() {
        $this->reset();
        return $this->redirect(['index']);
    }

    public function actionDelete($id) {
        Yii::$app->db->createCommand('DELETE FROM source_message WHERE id = :id', [
            ':id' => $id
        ])->execute();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Translation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Translation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Translation::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
