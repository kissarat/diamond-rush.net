<?php
/**
 * @link http://zenothing.com/
 */

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * @author Taras Labiak <kissarat@gmail.com>
 * This is the model class for table "type".
 *
 * @property integer $id
 * @property string $name
 * @property number $stake
 * @property number $income
 * @property number $pillow
 * @property number $profit
 * @property integer $degree
 * @property integer $next_id
 * @property integer $invest_id
 *
 * @property Type $next
 */
class Type extends ActiveRecord
{
    private static $_all;

    public static function tableName() {
        return 'type';
    }

    public function rules() {
        return [
            [['name'], 'string'],
            [['stake', 'income'], 'required'],
            [['stake', 'income', 'pillow'], 'number', 'min' => 0],
            [['next_id', 'degree'], 'integer', 'min' => 0]
        ];
    }

    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'stake' => Yii::t('app', 'Stake'),
            'income' => Yii::t('app', 'Income'),
            'pillow' => Yii::t('app', 'Pillow'),
            'next_id' => Yii::t('app', 'Next'),
            'degree' => Yii::t('app', 'Degree'),
        ];
    }

    /**
     * @return Type
     */
    public function getNext() {
        $types = static::all();
        return isset($types[$this->next_id]) ? $types[$this->next_id] : null;
    }

    /**
     * @return Type[]
     */
    public static function all() {
        /* @var $type Type */
        if (!static::$_all) {
            $types = static::find()->orderBy(['id' => SORT_ASC])->all();
            static::$_all = [];
            foreach($types as $type) {
                static::$_all[$type->id] = $type;
            }
        }
        return static::$_all;
    }

    /**
     * @param $id
     * @return Type|null
     */
    public static function get($id) {
        $types = Type::all();
        return isset($types[$id]) ? $types[$id] : null;
    }

    public function __toString() {
        return $this->name;
    }

    /**
     * @return array
     */
    public static function getItems() {
        $items = [];
        foreach(static::all() as $type) {
            $items[$type->id] = Yii::t('app', $type->name);
        }
        return $items;
    }
}
