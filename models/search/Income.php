<?php
/**
 * @link http://zenothing.com/
*/

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Income as IncomeModel;

/**
 * @author Taras Labiak <kissarat@gmail.com>
 * Income represents the model behind the search form about `app\models\Archive`.
 */
class Income extends IncomeModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'node_id', 'type_id', 'reinvest_from', 'time'], 'integer'],
            [['user_name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Income::find()
        ->with('node');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'node_id' => $this->node_id,
            'type_id' => $this->type_id,
            'reinvest_from' => $this->reinvest_from,
            'time' => $this->time
        ]);

        $query->andFilterWhere(['like', 'user_name', isset($params['user']) ? $params['user'] : $this->user_name]);

        return $dataProvider;
    }
}
