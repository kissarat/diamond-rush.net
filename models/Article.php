<?php
/**
 * @link http://zenothing.com/
*/

namespace app\models;

use app\behaviors\Journal;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "article".
 *
 * @property integer $id
 * @property string $title
 * @property string $content
 * @property integer $ip
 * @author Taras Labiak <kissarat@gmail.com>
 */
class Article extends ActiveRecord
{
    public function behaviors() {
        return [
            Journal::className()
        ];
    }

    public function rules()
    {
        return [
            [['title', 'content'], 'required'],
            [['content'], 'string', 'min' => 20],
            [['title'], 'string', 'min' => 3, 'max' => 255]
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'content' => Yii::t('app', 'Content'),
        ];
    }

    public function __toString() {
        return $this->title;
    }
}
