<?php
/**
 * @link http://zenothing.com/
*/

namespace app\models;

use app\Account;
use Exception;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "node".
 *
 * @property integer $id
 * @property string $user_name
 * @property integer $type_id
 * @property integer $count
 * @property integer $time
 * @property integer $reinvest_from
 *
 * @property Type $type
 * @property User $user
 * @property Node[] $children
 * @author Taras Labiak <kissarat@gmail.com>
 */
class Node extends ActiveRecord
{
    private $_children;

    public static function tableName() {
        return 'node';
    }

    public function rules() {
        return [
            [['user_name', 'type_id', 'count', 'time'], 'required'],
            [['type_id', 'count', 'time', 'reinvest_from'], 'integer', 'min' => 0],
            [['user_name'], 'string', 'max' => 24],
            ['time', 'default', 'value' => $_SERVER['REQUEST_TIME']]
        ];
    }

    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_name' => Yii::t('app', 'Username'),
            'type_id' => Yii::t('app', 'Plan'),
            'reinvest_from' => Yii::t('app', 'Reinvest From'),
            'count' => Yii::t('app', 'Counter'),
            'time' => Yii::t('app', 'Time'),
        ];
    }

    /**
     * @return User
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['name' => 'user_name']);
    }

    public function setUser(User $value) {
        $this->user_name = $value->name;
    }

    /**
     * @return Type
     */
    public function getType() {
        return $this->hasOne(Type::className(), ['id' => 'type_id']);
    }

    public function setType(User $value) {
        $this->type_id = $value->id;
    }

    public function getChildren() {
        if (!$this->_children) {
            $this->_children = Node::find()->where(['>', 'id', $this->id])->limit(40)->all();
        }
        return $this->_children;
    }

    public function countQueue() {
        return static::find()
            ->andWhere('time <= :time', [':time' => $this->time])
            ->andWhere(['type_id' => $this->type_id])
            ->sum('count');
    }

    public function decrement() {
        $this->count--;
        return $this->update(false, ['count']);
    }

    public static function rise() {
        $i = 0;
        $types = Type::all();
        foreach($types as $id => $type) {
            $previous = static::find()->where(['type_id' => $id])
                ->andWhere('count <= 0')
                ->all();
            foreach ($previous as $node) {
                $node->up();
                $i++;
            }
        }
        $pillow = Account::get('pillow');
        $number = ($pillow - $pillow % 70)/70;
        $expectant = static::getExpectant(1);
        if (!($number > 0 && $expectant)) {
            return $i;
        }
        $nodes = static::find()->where('type_id = 1')->orderBy(['time' => SORT_ASC,'id' => SORT_ASC])->limit($number)->all();
        foreach($nodes as $node) {
            $node->up();
            $i++;
        }
        $node = static::getExpectant(1);
        if ($expectant->count != $node->count) {
            $node->count = $expectant->count;
            if (!$node->update(false, ['count'])) {
                throw new Exception($node->dump());
            }
        }
        Account::add('pillow', - ($number * 70));
        return $i;
    }

    public function up() {
        Yii::$app->db->createCommand(
            'INSERT INTO "archive"(node_id,  user_name,  type_id,  reinvest_from, "time")
                          VALUES (:node_id, :user_name, :type_id, :reinvest_from, :time)', [
            ':node_id' => $this->id,
            ':user_name' => $this->user_name,
            ':type_id' => $this->type_id,
            ':reinvest_from' => $this->reinvest_from,
            ':time' => $this->time
        ])
            ->execute();
        $user = $this->user;
        $this->reinvest();
        if ($this->type->next_id) {
            $this->type_id = $this->type->next_id;
            if (!$this->invest()) {
                throw new Exception($this->dump());
            }
        }
        else {
            $this->delete();
        }

        if ($this->type->income) {
            $user->account += (float) $this->type->income;
            if (!$user->update(false, ['account'])) {
                throw new Exception($this->dump());
            }
        }
    }

    public function invest() {
        $this->count = $this->type->degree;
        $this->time = $_SERVER['REQUEST_TIME'];
        $expectant = $this->getExpectant($this->type_id);
        if ($expectant) {
            $expectant->decrement();
        }
        if ($this->save()) {
            Account::addBoth($this->type->profit, $this->type->pillow);
            return true;
        }
        return false;
    }

    /**
     * @param $type_id
     * @return Node
     */
    public static function getExpectant($type_id) {
        return static::find()->where(['type_id' => $type_id])->andWhere('count > 0')
            ->orderBy(['time' => SORT_ASC, 'id' => SORT_ASC])->limit(1)->one();
    }

    public function reinvest() {
        if ($this->type->invest_id) {
            for($i = 0; $i < $this->type->reinvest; $i++) {
                $node = new static([
                    'type_id' => $this->type->invest_id,
                    'user_name' => $this->user_name,
                    'reinvest_from' => $this->type_id
                ]);
                $node->invest();
            }
        }
    }

    public function dump() {
        $bundle = [
            'id' => $this->id,
            'type_id' => $this->type_id,
            'count' => $this->count,
            'time' => date($this->time),
            'user_name' => $this->user_name,
            'account' => $this->user->account
        ];
        if (count($this->errors) > 0) {
            $bundle['errors'] = $this->errors;
        }
        if (count($this->user->errors) > 0) {
            $bundle['user_errors'] = $this->user->errors;
        }
        return json_encode($bundle, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
    }

    public function __toString() {
        return $this->id . ' ' . Type::get($this->type_id);
    }
}
