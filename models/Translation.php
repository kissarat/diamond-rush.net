<?php
/**
 * @link http://zenothing.com/
*/

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "translation".
 *
 * @property integer $id
 * @property string $name
 * @property string $message
 * @property string $translation
 * @author Taras Labiak <kissarat@gmail.com>
 */
class Translation extends ActiveRecord
{
    public static function tableName() {
        return 'translation';
    }

    public static function primaryKey() {
        return ['id'];
    }

    public function rules() {
        return [
            [['id'], 'integer'],
            [['message', 'translation'], 'string'],
            ['name', 'string', 'max' => 24]
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => Yii::t('app', 'Name'),
            'message' => 'English',
            'translation' => 'Русский',
        ];
    }
}
