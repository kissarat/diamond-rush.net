<?php
/**
 * @link http://zenothing.com/
*/

$config = [
    'id' => 'gold-rush',
    'name' => 'Diamond Rush',
    'basePath' => __DIR__,
    'bootstrap' => ['log'],
    'defaultRoute' => 'home/index',
    'language' => empty($_COOKIE['lang']) ? 'ru' : 'en',
    'charset' => 'utf-8',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\ApcCache'
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                    'enabled' => false
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [
                'language/<code:\w{2}>' => 'home/language',
                'signup/<referral:\w+>' => 'user/signup',
                'profile/<name:[\w_\-\.]+>/edit' => 'user/update',
                'profile/<name:[\w_\-\.]+>' => 'user/view',
                'journal/user/<user:[\w_\-\.]+>' => 'journal/index',
                'journal/<id:\d+>' => 'journal/view',
                'password/<name:[\w_\-\.]+>' => 'user/password',
                'reset/<code:[\w_\-]+>' => 'user/password',
                'feedback/template/<template:\w+>' => 'feedback/create',
                'plan/<id:\d+>' => 'matrix/view',
                'open/<id:\d+>' => 'matrix/open',
                'investment/user/<user:[\w_\-\.]+>' => 'matrix/invest',
                'investment/<id:\d+>' => 'matrix/invest',
                'income/user/<user:[\w_\-\.]+>' => 'matrix/income',
                '<scenario:(withdraw|payment)>/user/<user:[\w_\-\.]+>' => 'invoice/index',
                '<scenario:(withdraw|payment)>/create' => 'invoice/create',
                '<scenario:(withdraw|payment)>' => 'invoice/index',
                'invoices/user/<user:[\w_\-\.]+>' => 'invoice/index',
                'invoice/<id:\d+>' => 'invoice/view',
                'text/<name:[\w_\-\.]+>' => 'translation/text',
                'account' => 'user/account',
                'contact' => 'feedback/create',
                'cabinet' => 'user/view',
                'users' => 'user/index',
                'investment' => 'matrix/invest',
                'plans' => 'matrix/plan',
                'invoices' => 'invoice/index',
                'queue' => 'matrix/index',
                'income' => 'matrix/income',
                'news' => 'article/index',
                'welcome' => 'home/index',
                'signup' => 'user/signup',
                'login' => 'user/login',
                'logout' => 'user/logout',
                'answers' => 'faq/index',
                'reviews' => 'review/index'
            ],
        ],
        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'app\MessageSource',
                    'enableCaching' => true
                ]
            ]
        ],
        'session' => [
            'class' => 'yii\web\CacheSession',
            'name' => 'auth'
        ],
    ],
    'params' => null,
];
