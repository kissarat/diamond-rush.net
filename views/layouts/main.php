<?php
/**
 * @link http://zenothing.com/
*/
use app\widgets\Alert;
use app\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
$login = Yii::$app->user->isGuest ? '' : 'login';
$manager = !Yii::$app->user->isGuest && Yii::$app->user->identity->isManager();
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <title><?= Html::encode($this->title) ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon"/>
    <link rel="image_src" href="/img/7.jpg" />
    <?= Html::csrfMetaTags() ?>
    <?php $this->head() ?>
</head>
<body>
<div class="background"></div>
<div class="navbar-border"></div>
<?php $this->beginBody() ?>
<div class="wrap <?= $login ?>">
    <header>
        <div class="auth-panel">
            <?php
            if (Yii::$app->user->isGuest) {
                echo Html::a(Yii::t('app', 'Signup'), ['user/signup']);
                echo Html::a(Yii::t('app', 'Login'), ['user/login']);
                echo Html::a(Yii::t('app', 'Forgot your password?'), ['user/request']);
            }
            else {
                echo Html::a(Yii::t('app', 'Profile'), ['user/view']);
                echo Html::a(Yii::t('app', 'Logout'), ['user/logout']);
            }
            ?>
        </div>
        <div class="brand">
            <?= Html::img('/img/7.jpg') ?>
            <div><span>D</span>IAMOND <span>R</span>USH</div>
        </div>
    </header>
    <?php
    NavBar::begin();

    $items = [
        ['label' => Yii::t('app', 'Home'), 'url' => ['/home/index'], 'options' => ['class' => 'hideable']],
        ['label' => Yii::t('app', 'Marketing'), 'url' => ['/matrix/plan']],
        ['label' => Yii::t('app', 'News'), 'url' => ['/article/index']],
        ['label' => Yii::t('app', 'FAQ'), 'url' => ['/faq/index']],
        ['label' => Yii::t('app', 'Feedback'), 'url' => ['/feedback/create']],
        ['label' => Yii::t('app', 'Advertisement'), 'url' => ['/home/banner']],
        ['label' => Yii::t('app', 'Reviews'), 'url' => ['/review/index']]
    ];

    if (!Yii::$app->user->isGuest) {
        $items[] = ['label' => Yii::t('app', 'Income'), 'url' => ['/matrix/income']];
        $items[] = ['label' => Yii::t('app', 'Investment'), 'url' => ['/matrix/invest']];
        $items[] = ['label' => Yii::t('app', 'Payment') , 'url' =>['/invoice/index']];
        if ($manager) {
            $items[] = ['label' => Yii::t('app', 'Queue'), 'url' => ['/matrix/index']];
            $items[] = ['label' => Yii::t('app', 'Translation') , 'url' => ['/translation/index']];
        }
    }

    if (Yii::$app->user->isGuest || !Yii::$app->user->identity->isManager()) {
        $items[] = empty($_COOKIE['lang'])
            ? ['label' => 'EN', 'url' => ['/home/language', 'code' => 'en'], 'options' => ['title' => 'English']]
            : ['label' => 'RU', 'url' => ['/home/language', 'code' => 'ru'], 'options' => ['title' => 'Русский']];
    }

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav'],
        'items' => $items,
    ]);
    NavBar::end();
    ?>


    <div class="container">
        <?= Breadcrumbs::widget([
            'homeLink' => false,
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<div id="metrika">
    <!-- Yandex.Metrika informer -->
    <a href="https://metrika.yandex.ru/stat/?id=31493068&amp;from=informer"
       target="_blank" rel="nofollow">
        <img src="https://mc.yandex.ru/informer/31493068/3_1_FFFFFFFF_FFFFFFFF_0_pageviews"
             title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)"
             alt="Яндекс.Метрика" />
    </a>
    <!-- /Yandex.Metrika informer -->
</div>

<footer class="footer">
    Разработано <a href="http://zenothing.com/">zenothing.com</a>
</footer>

<script src="/script.js"></script>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
