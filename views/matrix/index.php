<?php
/**
 * @link http://zenothing.com/
*/

use app\models\Type;
use app\widgets\Ext;
use yii\data\ArrayDataProvider;
use yii\grid\SerialColumn;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $models array */
/* @var $pages yii\data\Pagination */

$this->title = Yii::t('app', 'Queue');

$columns = [[
    'class' => SerialColumn::className(),
    'contentOptions' => ['class' => 'id']
]];
$keys = array_keys($models);
sort($keys);

$types = Type::all();
foreach($keys as $i) {
    $columns[] = [
        'attribute' => $i + 1,
        'label' => $types[$i + 1],
        'format' => 'html',
        'contentOptions' => function($row) use ($i) {
            return isset($row[$i]) && !Yii::$app->user->isGuest && Yii::$app->user->identity->isManager()
                ? ['data-id' => $row[$i]['id']]
                : [];
        },
        'value' => function($row) use ($i) {
            if (isset($row[$i])) {
                $invest = $row[$i];
                $cell = Html::a($invest['user_name'], ['invest', 'id' => $invest['id']]);
                if ($invest['reinvest_from']) {
                    $cell .= ' <span class="reinvest"></span>';
                }
                $count = $invest['count'];
                if ($count > 1) {
                    $cell .= " <span class='count'>$count</span>";
                }
                return $cell;
            }
            else {
                return '';
            }
        }
    ];
}

$transposed = [];
$types_length = count($types);
foreach($models as $i => $column) {
    foreach($column as $j => $cell) {
        $transposed[$j][$i] = $cell;
    }
}
$order = empty($_GET['order']) || 'asc' == $_GET['order'];
?>
<div class="matrix">
    <?= Ext::stamp() ?>
    <div class="control">
        <?php
        $links = [Html::tag('label', Yii::t('app', 'Page size') . ':')];
        $page_list = [15, 25, 50, 100];
        if (!Yii::$app->user->isGuest && Yii::$app->user->identity->isManager()) {
            $page_list = array_merge($page_list, [250, 500, 1000]);
        }
        foreach($page_list as $size) {
            $links[] = ($size == $pages->pageSize ? $size
                : Html::a($size, ['index', 'size' => $size, 'order' => $order ? 'asc' : 'desc']));
        }
        echo Html::tag('script', implode(' ', $links), [
            'type' => 'text/plain',
            'data-selector' => '#page-size'
        ]);

        echo Html::tag('script', implode(' ', [
            Html::tag('label', Yii::t('app', 'show') . ':'),
                $order
                ? Html::a(Yii::t('app', 'end of queue'), ['index', 'order' => 'desc', 'size' => $pages->pageSize])
                : Html::a(Yii::t('app', 'start of the queue'), ['index', 'order' => 'asc', 'size' => $pages->pageSize])
        ]), [
            'type' => 'text/plain',
            'data-selector' => '#sort-order'
        ]);
        ?>
        <p>
            <span id="page-size"></span>
            <span id="sort-order"></span>
        </p>

        <?php
        if (!Yii::$app->user->isGuest && Yii::$app->user->identity->isManager()) {
            echo Html::tag('div', Html::a(Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']),
                ['class' => 'form-group']);
        }
        ?>

        <?= LinkPager::widget([
            'pagination' => $pages,
        ]); ?>
    </div>

    <?= GridView::widget([
        'dataProvider' => new ArrayDataProvider([
            'allModels' => $transposed,
            'pagination' => false
        ]),
        'columns' => $columns
    ]); ?>

</div>
