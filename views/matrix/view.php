<?php
/**
 * @link http://zenothing.com/
*/

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Type */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Plans'), 'url' => ['plan']];
$this->params['breadcrumbs'][] = $this->title;
$manage = !Yii::$app->user->isGuest && Yii::$app->user->identity->isManager();
?>
<div class="type-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="form-group">
        <?php
        $form = ActiveForm::begin(['action' => ['open', 'id' => $model->id]]);
        echo Html::button(Yii::t('app', 'Open'), ['type' => 'submit', 'class' => 'btn btn-success']);
        ActiveForm::end();
        ?>
    </div>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'name',
                'format' => 'html',
                'value' => Html::a($model, ['view', 'id' => $model->id])
            ],
            'stake',
            'income',
            [
                'attribute' => 'next_id',
                'format' => 'html',
                'value' => $model->next_id ? Html::a($model->next, ['view', 'id' => $model->next_id]) : null
            ]
        ],
    ]);

    if ($manage) {
        $form = ActiveForm::begin();
        echo $form->field($model, 'pillow');
        echo Html::submitButton(Yii::t('app', 'Save'));
    }
    ?>

</div>
