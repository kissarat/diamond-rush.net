<?php
/**
 * @link http://zenothing.com/
*/

use app\God;
use app\models\Type;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $participants array */

$this->title = Yii::t('app', 'Plans');

function plan($model) {
    $name = Yii::t('app', $model->name);
    if (!Yii::$app->user->isGuest && Yii::$app->user->identity->isManager()) {
        $name = Html::a($name, ['view', 'id' => $model->id]);
    }
    return
        Html::img("/img/$model->id.jpg") .
        $name;
}

$columns = [
    [
        'attribute' => 'name',
        'format' => 'html',
        'contentOptions' => [
            'class' => 'plan'
        ],
        'value' => function($model) {
            return plan($model);
        }
    ],
    [
        'attribute' => 'stake',
        'value' => function(Type $model) {
            return $model->stake ? '$' . ((int) $model->stake) : '';
        }
    ],
    [
        'attribute' => 'income',
        'value' => function(Type $model) {
            return $model->income ? '$' . ((int) $model->income) : '';
        }
    ],
    'degree',
    [
        'label' => Yii::t('app', 'Reinvest'),
        'format' => 'html',
        'contentOptions' => [
            'class' => 'plan'
        ],
        'value' => function(Type $model) {
            return $model->invest_id ? plan(Type::get($model->invest_id)) : '';
        }
    ],
    [
        'attribute' => 'pillow',
        'value' => function(Type $model) {
            return $model->pillow ? '$' . ((int) $model->pillow) : '';
        }
    ],
    [
        'attribute' => 'next_id',
        'format' => 'html',
        'contentOptions' => [
            'class' => 'plan'
        ],
        'value' => function($model) {
            if ($model->next_id) {
                return plan($model->next);
            }
            else {
                return '';
            }
        }
    ],
    [
        'attribute' => 'id',
        'label' => Yii::t('app', 'Participants'),
        'value' => function($model) use ($participants) {
            return $participants[$model->id];
        }
    ]
];

if (!Yii::$app->user->isGuest) {
    $columns[] = [
        'label' => Yii::t('app', 'Action'),
        'format' => 'html',
        'value' => function($model) {
            if(Yii::$app->user->identity->account >= $model->stake) {
                return Html::a('Open', ['view', 'id' => $model->id], ['class' => 'btn btn-success btn-sm']);
            }
            else {
                return 'Недостаточно средств';
            }
        }
    ];
}
?>
<div class="type-index">

    <h1 class="bagatelle"><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'summary' => '',
        'columns' => $columns
    ]); ?>

    <p>
        <?= God::block('marketing') ?>
    </p>
    <p>
        <?= Yii::t('app', 'Referral bonus is 7.5%'); ?>
    </p>
</div>
