<?php
use yii\helpers\Html;
$sizes = [
    125 => 125,
    468 => 60,
    728 => 90
];
function banner($size) {
    $origin = 'http://' . $_SERVER['HTTP_HOST'];
    $url = $origin . "/img/$size.gif";
    return Html::a(Html::img($url, [
            'alt' => Yii::$app->name
        ]) .
        Html::script('', ['data-src' => 'http://zenothing.com/statistics.js']),
        $origin, ['target' => '_blank']);
}
?>
<div class="banners">
    <h1><?= Yii::t('app', 'Advertisement') ?></h1>
    <?php foreach([125, 468, 728] as $size): ?>
        <div id="<?= $size ?>">
            <h2><?= $size . 'x' . $sizes[$size] ?></h2>
            <?= banner($size) ?>
            <div class="form-group">
                <label for="<?= 'control' . $size ?>"><?= Yii::t('app', 'Code') ?>:</label>
                    <textarea id="<?= 'control' . $size ?>" class="form-control" rows="3"
                              style="font-family: courier"><?= htmlspecialchars(str_replace('data-src', 'src', banner($size))) ?></textarea>
            </div>
        </div>
    <?php endforeach; ?>
</div>
