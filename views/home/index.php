<?php
/**
 * @link http://zenothing.com/
 */
use app\God;
use app\widgets\Ext;
use yii\helpers\Html;

/**
 * @var string $statistics
 */

$this->title = Yii::$app->name;
$this->registerMetaTag([
    'name' => 'description',
    'content' => God::text('welcome')
]);

?>
<div class="home-index">
    <?= Ext::stamp() ?>

    <div class="top">
        <div>
            <span>У</span>НИКАЛЬНЫЙ,
            <span>Л</span>ЕГКИЙ И
            <span>И</span>ННОВАЦИОННЫЙ МАРКЕТИНГ
        </div>
        <div>
            Попробуй работать в команде
            и ты обречен на успех!
        </div>
        <?= God::block('front') ?>
    </div>
    <div>
        <div class="interesting">
            <div>Мы заинтересованы в успехе каждого!!!</div>
            <div>
                <?= Html::a(Yii::t('app', 'Read more'), ['matrix/plan']) ?>
            </div>
            <div></div>
        </div>
        <div class="row">
            <div class="block">
                <div class="welcome">
                    <img src="/img/success-plant.jpg">
                    <div>
                        <h2>Добро пожаловать в команду!</h2>
                        <?= God::block('welcome') ?>
                    </div>
                </div>
                <?= God::block('description') ?>
                <div class="video">
                    <iframe
                        width="527"
                        height="324"
                        src="//www.youtube.com/embed/o4YH8TlJg7k"
                        frameborder="0"
                        allowfullscreen=""></iframe>
                </div>
                <?= God::block('start') ?>
                <div>
                    <?= Html::a(Yii::t('app', 'Join our team'), ['user/signup'], ['class' => 'button']) ?>
                </div>
            </div>
            <dl>
                <dt>Платежные системы</dt>
                <dd>
                    <img src="/img/perfectmoney.png" />
                </dd>
                <dt>Посещения</dt>
                <dd><?= $statistics ?></dd>
            </dl>
        </div>
    </div>
</div>
