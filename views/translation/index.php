<?php
/**
 * @link http://zenothing.com/
*/

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\Translation */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Translations');
?>
<div class="translation-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Translation'), ['create'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Create Text'), ['create', 'rich' => true], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Clear Cache'), ['reset'], ['class' => 'btn btn-warning']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'message:html',
            'translation:html',

            ['class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['class' => 'action']],
        ],
    ]); ?>

</div>
