<?php
/**
 * @link http://zenothing.com/
*/

use dosamigos\ckeditor\CKEditor;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Translation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="translation-form">

    <?php $form = ActiveForm::begin();

    $message = $form->field($model, 'message')->textarea(['rows' => 6]);
    $translation = $form->field($model, 'translation')->textarea(['rows' => 6]);
    if ($model->name || isset($_GET['rich'])) {
        $message->widget(CKEditor::class, ['preset' => 'full']);
        $translation->widget(CKEditor::class, ['preset' => 'full']);
    }
    ?>

    <?= $form->field($model, 'name') ?>

    <?= $message ?>

    <?= $translation ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
