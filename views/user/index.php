<?php
/**
 * @link http://zenothing.com/
*/

use app\models\User;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\User */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', Yii::$app->user->identity->isManager() ? 'Users' : 'Sponsors');

$columns = [
    [
        'attribute' => 'name',
        'format' => 'html',
        'value' => function(User $model) {
            return Html::a($model->name, ['view', 'name' => $model->name]);
        }
    ],
    'email:email',
    'account',
    [
        'attribute' => 'perfect',
        'label' => Yii::t('app', 'Wallet')
    ],
    'skype',
    [
        'attribute' => 'ref_name',
        'format' => 'html',
        'value' => function(User $model) {
            return $model->ref_name ? Html::a($model->ref_name, ['view', 'name' => $model->ref_name]) : null;
        }
    ]
];

if (!Yii::$app->user->isGuest && Yii::$app->user->identity->isAdmin()) {
    $columns[] = [
        'format' => 'html',
        'contentOptions' => ['class' => 'action'],
        'value' => function(User $model) {
            return Html::a('', ['update', 'name' => $model->name], ['class' => 'glyphicon glyphicon-pencil']);
        }
    ];
}
?>
<div class="user-index">

    <h1 class="bagatelle"><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create User'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $columns,
    ]); ?>

</div>
