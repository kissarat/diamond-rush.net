<?php
/**
 * @link http://zenothing.com/
 */
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
?>
<div class="account">
    <h1><?= Yii::t('app', 'Accounts') ?></h1>
    <?php $form = ActiveForm::begin(); ?>
    <table class="table">
        <?php
        foreach($model as $key => $value) {
            echo Html::tag('tr',
                Html::tag('td', Yii::t('app', $key)) .
                Html::tag('td', $value, ['data-name' => $key])
            );
        }
        ?>
    </table>
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary']); ?>
    <?= Html::a('Двигать матрицу', ['/matrix/rise'], ['class' => 'btn btn-primary']) ?>
    <?php ActiveForm::end(); ?>
</div>
