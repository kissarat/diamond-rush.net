<?php
/**
 * @link http://zenothing.com/
 */

namespace app\components;


use PDO;
use Yii;
use yii\base\Component;
use ZipArchive;

class Backup extends Component {
    public function export($table, $columns, $seq = false) {
        if (is_array($columns)) {
            $columns = implode(', ', $columns);
        }

        $command = Yii::$app->db->createCommand("SELECT $columns FROM $table");
        $command->execute();
        $insert = [];
        while($row = $command->pdoStatement->fetch(PDO::FETCH_NUM)) {
            foreach($row as $i => $cell) {
                if (is_null($cell)) {
                    $row[$i] = 'NULL';
                }
                elseif (is_string($cell)) {
                    $cell = str_replace("'", "''", $cell);
                    $row[$i] = "'$cell'";
                }
            }

            $insert[] = '(' . implode(',', $row) . ')';
        }
        if (empty($insert)) {
            return '';
        }
        else {
            $sql = ["INSERT INTO $table ($columns) VALUES\n" . implode(",\n", $insert)];
            if ($seq) {
                if (true === $seq) {
                    $seq = $table . '_id_seq';
                }
                $command = Yii::$app->db->createCommand("SELECT nextval('$seq')");
                $command->execute();
                $seq_val = (int) $command->pdoStatement->fetchColumn();
                $sql[] = "SELECT setval('$seq', $seq_val)";
            }
            return implode(";\n\n", $sql);
        }
    }

    public function compress($name, $files) {
        $zip = new ZipArchive();
        $zipfilename = date('y-m-d-H-', time()) . $name;
        $webpath = "/export/$zipfilename.sql.zip";
        $zipname = Yii::$app->basePath . '/web' . $webpath;
        if (true !== $zip->open($zipname, ZipArchive::CREATE)) {
            return false;
        }
        foreach($files as $filename => $content) {
            if (!empty(trim($content))) {
                $zip->addFromString($filename, $content);
            }
        }
        $zip->close();
        return $webpath;
    }
}
